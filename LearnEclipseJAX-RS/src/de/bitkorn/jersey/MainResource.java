package de.bitkorn.jersey;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.LoggerConfig;

public class MainResource {
	protected static final Logger LOGGER = LogManager.getLogger(LoggerConfig.RootLogger.class);

	private final String apiKey = "somekey73546hduehe";
	
	protected boolean canAccess(String apiKey) {
		LOGGER.info("apiKey     : " + apiKey);
		LOGGER.info("this.apiKey: " + this.apiKey);
//		return true;
		if(apiKey.equals(this.apiKey)) {
			return true;
		} else {
			return false;
		}
//		return (apiKey == this.apiKey);
	}
}
