package de.bitkorn.jersey.bean;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.JsonObject;

public class CustomerBean implements JsonString {

	private final JsonObjectBuilder jsonObjBldr;
	private JsonObject jsonObj;
    private int id;
    private String name;

	public CustomerBean() {
		this.jsonObjBldr = Json.createObjectBuilder();
	}

	@Override
	public String getString() {
		this.jsonObj = this.jsonObjBldr.build();
		return this.jsonObj.toString();
	}

	@Override
	public CharSequence getChars() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public ValueType getValueType() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        this.jsonObjBldr.add("name", name);
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
        this.jsonObjBldr.add("id", id);
	}
    
}
