package de.bitkorn.jersey.resource;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.LoggerConfig;

import javax.ws.rs.Produces;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import de.bitkorn.jersey.MainResource;
import de.bitkorn.jersey.bean.CustomerBean;

/**
 * http://localhost:8080/LearnEclipseJAX-RS/rest/customer/42
 * 
 * @author allapow
 *
 */
@Path("customer")
public class CustomerResource extends MainResource {

	/**
	 * Retrieves representation of an instance of de.bitkorn.jaxrs.BookResource
	 * 
	@HeaderParam("apikey")
	 * @param id
	 * @return an instance of java.lang.String
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}/")
	public String getJson(@Context HttpServletResponse response, @HeaderParam("apikey") String key, @PathParam("id") int id) {
		if(!canAccess(key)) {
			LOGGER.info("canAccess(key): " + canAccess(key));
			try {
				response.sendError(401);
			} catch (IOException e) {
				LOGGER.error(e.getMessage());
			}
			return "access denied";
		}
		CustomerBean customer = new CustomerBean();
		customer.setId(id);
		customer.setName("Allapow Okey");
		LOGGER.info("Hello from " + this.getClass().getName());
		return customer.getString();
	}

}
