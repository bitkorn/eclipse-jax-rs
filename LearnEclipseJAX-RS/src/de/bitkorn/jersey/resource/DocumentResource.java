package de.bitkorn.jersey.resource;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.StreamingOutput;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.LoggerConfig;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import de.bitkorn.jersey.MainResource;
import de.bitkorn.service.FileAccessService;

/**
 * http://localhost:8080/LearnEclipseEE02/rest/document/42
 * 
 * @author allapow
 *
 */
@Path("document")
public class DocumentResource extends MainResource {

	private FileAccessService fileService;

	public DocumentResource() {
		fileService = new FileAccessService();
	}

	/**
	 * Retrieves representation of an instance of de.bitkorn.jaxrs.BookResource
	 * 
	 * @HeaderParam("apikey")
	 * 
	 * @param id
	 * @return an instance of java.lang.String
	 */
	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("{id}/")
	public StreamingOutput getJson(@Context HttpServletResponse response, @Context ServletContext ctx, @HeaderParam("apikey") String key,
			@PathParam("id") int id) {
		if (!canAccess(key)) {
			LOGGER.info("canAccess(key): " + canAccess(key));
			try {
				response.sendError(401);
			} catch (IOException e) {
				LOGGER.error(e.getMessage());
			}
			return null;
		}
		return new StreamingOutput() {

			@Override
			public void write(OutputStream output) throws IOException, WebApplicationException {
				output.write(fileService.getDocumentSource());
//				output.write("hello world\n".getBytes());
//				output.write(("BaseFilePath: " + fileService.getBaseFilePath() + "\n").getBytes());
//				output.write(("File CanonicalPath: " + new File(".").getCanonicalPath() + "\n").getBytes());
//				output.write(("getContextPath: " + ctx.getContextPath() + "\n").getBytes());
//				output.write(("File AbsolutePath: " + new File(".").getAbsolutePath() + "\n").getBytes());
//				output.write(("File Path: " + new File(".").getPath() + "\n").getBytes());
			}
		};

	}

}
