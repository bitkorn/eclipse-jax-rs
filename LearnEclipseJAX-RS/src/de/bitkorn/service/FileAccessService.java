package de.bitkorn.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.CodeSource;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.LoggerConfig;

public class FileAccessService {
	protected static final Logger LOGGER = LogManager.getLogger(LoggerConfig.RootLogger.class);
	public static final String FILE_SEPERATOR = "/";
	private String baseFilePath;
	private Properties prop = new Properties();
	private InputStream input = null;

	public FileAccessService() {
		computeBaseFilePath();
	}

	private void computeBaseFilePath() {
		CodeSource cs = FileAccessService.class.getProtectionDomain().getCodeSource();
		try {
			baseFilePath = cs.getLocation().toURI().getPath();
			// System.out.println("CodeSource: " + myPath);
		} catch (URISyntaxException ex) {
			LOGGER.error(ex.getMessage());
		}
		// absolute path plus trailing slash
		baseFilePath = baseFilePath.substring(0, baseFilePath.lastIndexOf(FILE_SEPERATOR) + 1);
		try {
			input = new FileInputStream(baseFilePath + "config.properties");
			prop.load(input);
		} catch (FileNotFoundException ex) {
			LOGGER.error(ex.getMessage());
		} catch (IOException ex) {
			LOGGER.error(ex.getMessage());
		}
	}

	public String getBaseFilePath() {
		return baseFilePath;
	}

	public byte[] getDocumentSource() {
		try {
			LOGGER.info("File CanonicalPath: " + new File(".").getCanonicalPath() + "\n");
		} catch (IOException ex) {
			LOGGER.error(ex.getMessage());
		}
		LOGGER.info("File AbsolutePath: " + new File(".").getAbsolutePath() + "\n");
		LOGGER.info("File Path: " + new File(".").getPath() + "\n");

		Path path = Paths.get(prop.getProperty("folder_tmp") + "/test.pdf");
		try {
			return Files.readAllBytes(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Properties getProp() {
		return prop;
	}
	
	
}
